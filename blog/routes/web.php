<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'IndexController@index' );


// Route::get('/register', function(){
//     return view('register');
// });

Route::get('/register', 'AuthController@regis');

// Route::get('/welcomee', function(){
//     return view('welcomee');
// });

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.datatable');
});
Route::get('/table', function(){
    return view('table.table');
});
